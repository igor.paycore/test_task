{{- define "logstash.name" -}}
logstash
{{- end }}
{{- define "logstash.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := include "logstash.name" . }}
{{- if .Chart.Name }}
{{- .Chart.Name }}-{{- $name }}
{{- else }}
{{- $name }}
{{- end }}
{{- end }}
{{- end }}